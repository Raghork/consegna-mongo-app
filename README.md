# Consegna MongoDB Atlas

In questa consegna vi è la parte del database e dell'applicazione.
Il database è dei Pokémon della prima generazione (Jhoto).


## Analisi del database e dei metodi

Questo database è stato creato con Mongo DB Atlas e una Stitch App.
Nella parte di Mongo DB vi sono tutti i dati del database, mentre per 
quanto riguarda Stitch vi è il metodo GET dei CRUD per ottenere i Pokémon.

## Analisi dell'applicazione

L'applicazione è stata creata con Flutter e si connette alla collection di 
Mongo.
Dalla applicazione si potranno vedere i Pokémon della prima regione, con la
descrizione, il tipo e il metodo di evoluzione.

Flutter is Google’s mobile app SDK for crafting high-quality native interfaces 
on iOS and Android in record time. Flutter works with existing code, is used by
developers and organizations around the world, and is free and open source.

Here are the important parts from that statements: 

 1.High-quality native interface
 
 2.iOS and Android in record time
 
 3.Flutter speeds up development by quite a bit
 
 4.Easy-to-learn development language for existing mobile developers
 
 5.Fluid animations and beautiful UI
 
 6.Allows Android + iOS apps from the same codebase

# Link consegna: [Consegna](https://gitlab.com/zuccante5IC/consegna190326)
# Link wiki: [Wki](https://gitlab.com/Raghork/consegna/wikis/consegn

