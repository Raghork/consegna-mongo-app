class PokeHub {
  List<Pokemon> pokemon;

  PokeHub({this.pokemon});

  PokeHub.fromJson(Map<String, dynamic> json) {
    if (json['pokemon'] != null) {
      pokemon = new List<Pokemon>();
      json['pokemon'].forEach((v) {
        pokemon.add(new Pokemon.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.pokemon != null) {
      data['pokemon'] = this.pokemon.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Pokemon {
  String sId;
  int pkdxId;
  int nationalId;
  String name;
  int iV;
  String description;
  String img;
  List<String> types;
  List<Evolutions> evolutions;

  Pokemon(
      {this.sId,
        this.pkdxId,
        this.nationalId,
        this.name,
        this.iV,
        this.description,
        this.img,
        this.types,
        this.evolutions});

  Pokemon.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    pkdxId = json['pkdx_id'];
    nationalId = json['national_id'];
    name = json['name'];
    iV = json['__v'];
    description = json['description'];
    img = json['img'];
    types = json['types'].cast<String>();
    if (json['evolutions'] != null) {
      evolutions = new List<Evolutions>();
      json['evolutions'].forEach((v) {
        evolutions.add(new Evolutions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['pkdx_id'] = this.pkdxId;
    data['national_id'] = this.nationalId;
    data['name'] = this.name;
    data['__v'] = this.iV;
    data['description'] = this.description;
    data['img'] = this.img;
    data['types'] = this.types;
    if (this.evolutions != null) {
      data['evolutions'] = this.evolutions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Evolutions {
  String level;
  String method;
  String to;
  String sId;

  Evolutions({this.level, this.method, this.to, this.sId});

  Evolutions.fromJson(Map<String, dynamic> json) {
    level = json['level'];
    method = json['method'];
    to = json['to'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['level'] = this.level;
    data['method'] = this.method;
    data['to'] = this.to;
    data['_id'] = this.sId;
    return data;
  }
}