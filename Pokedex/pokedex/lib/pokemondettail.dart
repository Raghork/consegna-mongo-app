import 'package:flutter/material.dart';
import 'package:pokedex/pokemon.dart';

class PokeDetail extends StatelessWidget {
  final Pokemon pokemon;

  PokeDetail({this.pokemon});

  bodyWidget(BuildContext context) => Stack(
    children: <Widget>[
      Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(
              height: 70.0,
            ),
            Text(pokemon.name),
            Text("Description"),
            Text(pokemon.description),
            Text(""),
            Text(
              "Types",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: pokemon.types
                  .map((t) => FilterChip(
                  backgroundColor: Colors.blue,
                  label: Text(t),
                  onSelected: (b) {}))
                  .toList(),
            ),
            Text("Next Evolution Method",
                style: TextStyle(fontWeight: FontWeight.bold)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: pokemon.evolutions
                  .map((n) => FilterChip(
                  backgroundColor: Colors.blue,
                  label: Text(n.method,
                      style: TextStyle(color: Colors.white)),
                  onSelected: (b) {}))
                  .toList(),
            ),
            Text("Level",
                style: TextStyle(fontWeight: FontWeight.bold)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: pokemon.evolutions
                  .map((n) => FilterChip(
                  backgroundColor: Colors.blue,
                  label: Text(n.level,
                      style: TextStyle(color: Colors.white)),
                  onSelected: (b) {}))
                  .toList(),
            ),
            Text("To",
                style: TextStyle(fontWeight: FontWeight.bold)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: pokemon.evolutions
                  .map((n) => FilterChip(
                  backgroundColor: Colors.blue,
                  label: Text(n.to,
                      style: TextStyle(color: Colors.white)),
                  onSelected: (b) {}))
                  .toList(),
            ),
          ],
        ),
      ),
      Align(
        alignment: Alignment.topCenter,
        child: Hero(
            tag: pokemon.img,
            child: Container(
              height: 100.0,
              width: 100.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover, image: NetworkImage(pokemon.img))),
            )),
      )
    ],
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.grey,
        title: Text(pokemon.name),
      ),
      body: bodyWidget(context),
    );
  }
}